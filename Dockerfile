#
# NOTE: This is a version for dev.
#

FROM python:3.8.2-slim

# Set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
ENV MICRO_SERVICE=/opt/app
ENV LOG_DIR=/var/log/app

RUN apt-get update \
    && apt-get install -y cron mariadb-client make gcc libmariadb-dev-compat libmariadb-dev

COPY ./requirements/* /tmp/

RUN pip install --upgrade pip \
    && pip install -r /tmp/dev.txt

COPY . $MICRO_SERVICE
WORKDIR $MICRO_SERVICE

RUN mkdir -p /var/log/app

ENTRYPOINT ["sh", "/opt/app/entrypoint.sh"]

CMD ["make", "startup"]
