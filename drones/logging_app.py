from datetime import datetime

from pythonjsonlogger import jsonlogger

LOG_DATE_FIELD = '@timestamp'
LOG_DATE_FORMAT = '%Y-%m-%dT%H:%M:%S'
LOG_DATE_MICROSECONDS_PLACES = 3


class CustomJsonFormatter(jsonlogger.JsonFormatter):
    """
    Redefine Format for this library.

    See: https://github.com/madzak/python-json-logger.
    """

    def add_fields(self, log_record, record, message_dict):
        """
        Add custom fields to logging record.

        :param OrderedDict log_record: Fields to add to log.
        :param LogRecord record: Current log record instance.
        :param dict message_dict: Message to include in json log.
        :return: None.
        """
        super(CustomJsonFormatter, self).add_fields(
            log_record, record, message_dict
        )
        if not log_record.get(LOG_DATE_FIELD):
            # This doesn't use record.created, so it is slightly off.
            now_dt = datetime.utcnow()
            micro_seconds = now_dt.strftime('%f')
            log_record[LOG_DATE_FIELD] = '{}.{}Z'.format(
                now_dt.strftime(LOG_DATE_FORMAT),
                (
                    micro_seconds[:LOG_DATE_MICROSECONDS_PLACES]
                    if len(micro_seconds) > LOG_DATE_MICROSECONDS_PLACES
                    else micro_seconds
                ),
            )
