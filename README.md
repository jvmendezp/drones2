## Drones
There is a major new technology that is destined to be a disruptive force in the field of transportation: **the drone**. Just as the mobile phone allowed developing countries to leapfrog older technologies for personal communication, the drone has the potential to leapfrog traditional transportation infrastructure.

## Description
This service allows clients (via REST API) to communicate with the drones (i.e. **dispatch controller**). 
The specific communication with the drone is out side the scope of this solution.

**As problem requirements we have**:
1. Registering a drone.
1. Loading a drone with medication items.
1. Checking loaded medication items for a given drone.
1. Checking available drones for loading. 
1. Check drone battery level for a given drone.
1. Implement validations in order to meet business restrictions.
1. Introduce a periodic task to check drones battery levels and create history/audit event log for this.

**The solution contains:**
1. Support for [make](https://man7.org/linux/man-pages/man1/make.1.html) utility to execute all defined options easily. 
1. [REST API](https://restfulapi.net/) with required features built using [Django REST framework](https://www.django-rest-framework.org/). Available in http://localhost/v1/
1. [Swagger](https://swagger.io/) Interface with REST API information powered by [drf-yasg](https://drf-yasg.readthedocs.io/). Available in http://localhost/swagger/.
1. [MariaDb](https://mariadb.org/) as database storage.
1. [Cron](https://en.wikipedia.org/wiki/Cron) utility to execute periodic tasks.
1. Utility to create random data for testing powered by [Django-admin commands](https://docs.djangoproject.com/en/4.0/howto/custom-management-commands/).
1. All application components works using [Docker](https://www.docker.com/) containters.
1. [Docker-compose](https://docs.docker.com/compose/) specification for easy execution of all features.
1. Code quality assurance through:
    - [Django tests](https://docs.djangoproject.com/en/4.0/topics/testing/)
    - [SonarQube](https://www.sonarqube.org/) integration. It includes a client to analyze code and a local SonarQube server. Available in http://localhost:9000/.
    - Test coverage management using [coverage](https://pypi.org/project/coverage/) library. SonarQube reports 87.8% of coverage and category A for all measures.
    - [flake8](https://pypi.org/project/flake8/) for python code style guide (see ``.flake`` archive).
    - [black](https://pypi.org/project/black/) for python code formatter.
    - [isort](https://pypi.org/project/isort/) (see ``.isort`` archive).
1. Log managment through an [Elastic Stack](https://www.elastic.co/what-is/elk-stack) with:
    - [ElasticSearch](https://www.elastic.co/what-is/elasticsearch) search and analytics engine for log data. 
    - [Logstash](https://www.elastic.co/guide/en/logstash/current/introduction.html) to ingest and process data.
    - [Filebeat](https://www.elastic.co/beats/filebeat) to collect log from application records.
    - [Kibana](https://www.elastic.co/kibana/) to visualize ElasticSearch data. Available in http://localhost:5601/.
1. [Jupyter](https://jupyter.org/) notebooks with an example of each of required endpoints. Please, see:
    - [Jupyter notebooks](https://gitlab.com/jvmendezp/drones2/-/tree/main/data/jupyter)
    - [Examples of executions](https://gitlab.com/jvmendezp/drones2/-/tree/main/data/jupyter/examples)
1. Check known security vulnerabilities for dependencies powered by [Safety](https://pypi.org/project/safety/). 

And, that's all. :smiley:

## Installation
Please, verify if you have each of following items using your linux terminal (without version specification):

1. [Docker-compose](https://docs.docker.com/compose/)
   ```
   > docker-compose -v
   docker-compose version 1.25.5, build unknown
   ``` 
1. [make](https://man7.org/linux/man-pages/man1/make.1.html)
   ```
    > make -v
    GNU Make 4.2.1
    Este programa fue construido para x86_64-suse-linux-gnu
    Copyright (C) 1988-2016 Free Software Foundation, Inc.
    Licencia GPLv3+: GNU GPL versión 3 o posterior <http://gnu.org/licenses/gpl.html>
    Este es software libre: cualquiera es libre para redistribuirlo y modificarlo.
    No existe GARANTÍA ALGUNA, hasta los límites permitidos por las leyes aplicables.
   ```

This application uses a ``.env`` file to setup all available options:
1. Create a copy of ``.env.example``
1. Rename new file as ``.env``
1. Update values.

Please, use ``root`` user in database connection in order to have permissions to create new database on test functionality.

You should have availability in your local configuration to register automatically a new docker network.

## Usage

This solution allows following options using ``make`` and ``docker-compose`` utilities.

1. **Create data for testing:**
   ```commandline
   make docker_test_data
   ```
   This command will create and return necessary data to test created endpoints:
    1. Valid JWT for user authentication to use in all endpoints with ``Authorization`` header. Example: ``Bearer JWT``
    1. Create available drone for loading medicament.
    1. Create a new drone and load it with medicament.

    Execute this command many times as you want. It always will return fresh data.    
1. **Start REST API:**
   ```commandline
   make docker_app
   ```
   This command will start containers for: ``REST API``, ``Cron``, ``Database storage``
1. **Start all services:**
   ```commandline
   make docker_all
   ```
   This command will start containers for: ``REST API``, ``Cron``, ``Database storage``, ``Filebeat``, ``Logstash``, ``Elasticsearch`` and ``Kibana``.

1. **Only start Elastic solution:**
   ```commandline
   make docker_elk
   ```
   This command will start containers for: ``Filebeat``, ``Logstash``, ``Elasticsearch`` and ``Kibana``.
1. **Run application tests:**
   ```commandline
   make docker_test
   ```
   This command will start containers for: ``REST API``, ``Cron``, ``Database storage``, execute tests and finish printing results.
1. **Run SonarQube analysis (include coverage):**
   ```commandline
   make docker_sonar
   ```
   This command will start containers for ``SonarQube server`` and ``SonarQube client`` to create an update of code status.
1. **Run security analysis of python dependencies:**
   ```commandline
   make docker_safety
   ```
   This command will start containers for: ``REST API``, check dependencies vulnerabilities and print results.

## Roadmap
- [ ] Define user roles and permissions.
- [ ] Create more documentation using Sphinx library.
- [ ] Change to database storage with support for geospatial data like [MongoDb](https://www.mongodb.com/) or [postgis](https://postgis.net/).
- [ ] Improve logic with async functions.
- [ ] Implement CI/CD.
- [ ] Increase test coverage.
- [ ] Update architecture to be more close to DDD architecture (Avoid fat classes).
- [ ] Move validation logic from serializers (app layer) to services to be available for all application logic.
- [ ] Try to use [dataclass](https://docs.python.org/3/library/dataclasses.html) + [pydantic](https://pydantic-docs.helpmanual.io/) as domain models and [DTO](https://www.okta.com/identity-101/dto/).
- [ ] Document architecture and development framework.
- [ ] Create reports in Kibana.
- [ ] Integrate [Grafana] to create nice dashboards (https://grafana.com/).
- [ ] Check with clients advantages of [GraphQL](https://graphql.org/)
- [ ] Update to latest LTS python version.
- [ ] Update code configurations(includes docker-compose) for prod environment.


## Screenshots and examples
1. **Available ``make`` options**
   
   You can see all available options for make command. Execute: ``make``
   ![Swagger](./data/readme/screenshots/make.png)*See make options.*
1. **Swagger**
   
   You can access to Swagger after start REST API, using following url: http://localhost/swagger/
   ![Swagger](./data/readme/screenshots/swagger.png)*Main Swagger view. You can see available REST endpoints.*
1. **See drone battery levels from history:**
   
   We can see logs records with this in log: ``drone_audit.log`` (location will depend of log directory configuration):
   
   ```log
   {"levelname": "INFO", "process": 247, "threadName": "MainThread", "name": "CreateBatteryEventLogging", "lineno": 70, "message": "DroneStatus id: 134, serial_number: SERIAL_YkRpUwVgQyEg, battery_percent: 89, status: LOADED, model: Lightweight", "@timestamp": "2022-03-21T00:00:03.166Z"}
   {"levelname": "INFO", "process": 247, "threadName": "MainThread", "name": "CreateBatteryEventLogging", "lineno": 70, "message": "DroneStatus id: 135, serial_number: SERIAL_UXxTWuQYBjjy, battery_percent: 55, status: LOADED, model: Lightweight", "@timestamp": "2022-03-21T00:00:03.166Z"}
   {"levelname": "INFO", "process": 247, "threadName": "MainThread", "name": "CreateBatteryEventLogging", "lineno": 70, "message": "DroneStatus id: 136, serial_number: SERIAL_wnxHcQUGBgmM, battery_percent: 36, status: LOADED, model: Lightweight", "@timestamp": "2022-03-21T00:00:03.166Z"}
   {"levelname": "INFO", "process": 247, "threadName": "MainThread", "name": "CreateBatteryEventLogging", "lineno": 70, "message": "DroneStatus id: 137, serial_number: SERIAL_AwFvexhDNuaU, battery_percent: 50, status: LOADED, model: Lightweight", "@timestamp": "2022-03-21T00:00:03.166Z"}
   ```
   
   or check them in Kibana (http://localhost:5601/)
   ![Swagger](./data/readme/screenshots/kibana_battery.png)*See logs in Kibana (Creating index previously).*
1. **Create test data**

   You can create new data for testing using command: ``make docker_test_data``
   ![Swagger](./data/readme/screenshots/test_data.png)*Command execution example to create test data.*

1. **Jupyter Notebooks**
    
    You can use a **Jupyter** to consume REST endpoints. Following directory (``./data/jupyter``) includes one notebook for each created endpoint.
    
    This solution does not include a Jupyter application; but you can deploy your own locally with access to localhost. 
    ![Jupyter](./data/readme/screenshots/jupyter_1.png)*Example of Jupyter Notebook.*
1. **Django tests execution**

    ```
    > make docker_test
    docker-compose run drones_app make test
    Starting drones2_database_mariadb_1 ... done
    find . -type f -name "*.py[co]" -delete -or -type d -name "__pycache__" -delete
    python manage.py test
    Found 40 test(s).
    Creating test database for alias 'default'...
    System check identified no issues (0 silenced).
    ................WARNING django.request:241 Unauthorized: /v1/drones/
    (other texts)
    ...
    ----------------------------------------------------------------------
    Ran 40 tests in 8.386s
    
    OK
    Destroying test database for alias 'default'...     
    ```
1. SonarQube analysis
   
   First start SonarQube local server:    
    
    ```commandline
    make docker_sonar
    ```
    
    Go to http://localhost:9000/ and create project and setup ``SONAR_LOGIN`` environment var in ``.env`` file.
    
    Second, execute SonarQube analysis:
    ```commandline
    make docker_sonar_analysis
    ```
    This command will run SonarQube analysis (include coverage) and store results in SonarQube server:
    ![SonarQube](./data/readme/screenshots/sonarqube.png)*Summary of results of SonarQube analysis.*
1. Search for dependencies vulnerabilities:
    ```commandline
    make docker_safety
    ```
   This command will show following (Good results!):
   ```commandline
    (drones2) > make docker_safety
    docker-compose run drones_app make safety
    Starting drones2_database_mariadb_1 ... done
    python -m safety check -r requirements/prod.txt
    +==============================================================================+
    |                                                                              |
    |                               /$$$$$$            /$$                         |
    |                              /$$__  $$          | $$                         |
    |           /$$$$$$$  /$$$$$$ | $$  \__//$$$$$$  /$$$$$$   /$$   /$$           |
    |          /$$_____/ |____  $$| $$$$   /$$__  $$|_  $$_/  | $$  | $$           |
    |         |  $$$$$$   /$$$$$$$| $$_/  | $$$$$$$$  | $$    | $$  | $$           |
    |          \____  $$ /$$__  $$| $$    | $$_____/  | $$ /$$| $$  | $$           |
    |          /$$$$$$$/|  $$$$$$$| $$    |  $$$$$$$  |  $$$$/|  $$$$$$$           |
    |         |_______/  \_______/|__/     \_______/   \___/   \____  $$           |
    |                                                          /$$  | $$           |
    |                                                         |  $$$$$$/           |
    |  by pyup.io                                              \______/            |
    |                                                                              |
    +==============================================================================+
    | REPORT                                                                       |
    | checked 12 packages, using free DB (updated once a month)                    |
    +==============================================================================+
    | No known security vulnerabilities found.                                     |
    +==============================================================================+
    ```
