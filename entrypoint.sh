#!/bin/sh

#fix link-count, as cron is being a pain, and docker is making hardlink count >0 (very high)

env >> /etc/environment

if [ "$1" = cron ]; then
  ./manage.py crontab add
fi

if [ "$1" = drones_app ]; then
  make startup
fi

# Hand off to the CMD
exec "$@"