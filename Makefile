.PHONY: help,clean,run,migration,migrate,test,pip_dev,pip_prod,pip_test,safety
.DEFAULT_GOAL := help

SHELL:= /bin/bash

help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

clean: ## Remove compile python files.
	find . -type f -name "*.py[co]" -delete -or -type d -name "__pycache__" -delete

run:clean  ## Start dev local server.
	python manage.py runserver 0.0.0.0:8000

crontab:clean  ## Reload crontab configuration.
	python manage.py crontab remove
	python manage.py crontab add

migration:clean  ## Create file migrations from models configuration.
	python manage.py makemigrations

migrate:clean  ## Execute migrations.
	python manage.py migrate

static:
	 python manage.py  collectstatic

startup: migrate static run  ## Execute migrations and start local server.

test:clean  ## Execute tests for all project.
	python manage.py test

pip_dev:  ## Install dependencies for dev environment.
	pip install -r requirements/dev.txt

pip_prod:  ## Install dependencies for prod environment.
	pip install -r requirements/prod.txt

pip_test:  ## Install dependencies for test environment.
	pip install -r requirements/test.txt

safety:  ## Safety checks your installed dependencies for known security vulnerabilities.
	python -m safety check -r requirements/prod.txt

test_data:  ## Create a set of data.
	python manage.py  create_test_data

coverage:  ## Execute python tests `coverage` using unittests framework.
	coverage run --source='.' manage.py test
	coverage xml

########################
#  Docker commands     #
########################

docker_all:docker_test_data   ## Start all solution using containers.
	docker-compose -f docker-compose.yml up drones_app cron database_mariadb logstash elasticsearch filebeat kibana

docker_app:docker_test_data  ## Start app and database using docker containers.
	docker-compose -f docker-compose.yml up drones_app cron database_mariadb

docker_elk:  ## Get up ELK applications: ElasticSearch Logstash Filebeat and Kibana.
	docker-compose -f docker-compose.yml up logstash elasticsearch filebeat kibana

docker_test_data:  ## Create data for testing.
	docker-compose run drones_app make test_data

docker_coverage:  ## Execute python tests `coverage` inside docker using unittests framework.
	docker-compose run drones_app make coverage

docker_test:  ## Execute test inside docker container.
	docker-compose run drones_app make test

docker_sonar:  ## Launch SonarQube server.
	docker-compose -f docker-compose.yml up sonarqube

docker_sonar_analysis:docker_coverage  ## Execute sonar scanner client and send results to SonarQube.
	docker-compose -f docker-compose.yml up sonar_scanner_cli

docker_safety:  ## Safety checks inside docker your installed dependencies for known security vulnerabilities.
	docker-compose run drones_app make safety