import logging

from django.contrib.auth.models import User
from rest_framework_simplejwt.tokens import RefreshToken
from typing import Generator

from medicaments.domain.models import Drone
from medicaments.domain.services import (
    ICreateBatteryEventLog,
    ICreateUserAuthTokens,
    IListDronesToCheckBattery,
)

logger = logging.getLogger(__name__)


class CreateUserAuthTokensWithSimpleJWT(ICreateUserAuthTokens):
    """Implementation to build auth token using rest_framework_simplejwt."""

    def execute(self, user: User) -> str:
        """
        Create a JWT token for specified user.

        :param User user: Target user.
        :return: Valid JWT for specified user.
        :rtype: str
        """
        refresh = RefreshToken.for_user(user=user)
        return refresh.access_token


class ListAllDronesToCheckBattery(IListDronesToCheckBattery):
    """Implementation to return a list of all drones."""

    def execute(self) -> Generator[Drone, None, None]:
        """
        Returns all drones as pending to check battery.

        :return: Drones to check battery.
        :rtype: Generator[Drone, None, None]
        """
        for drone in Drone.objects.all():
            yield drone


class CreateBatteryEventLogging(ICreateBatteryEventLog):
    """Implementation to add records using logging module."""

    def __init__(self):
        self.custom_logger = self.get_logger()

    @classmethod
    def get_logger(cls) -> logging.Logger:
        """
        Create a logger instance to use it to manage drone information.

        :return: Logger instance to add drone information.
        :rtype: logging.Logger
        """
        return logging.getLogger(cls.__name__)

    def execute(self, drone: Drone) -> bool:
        """
        Execute logic create event log using logging module.

        :param Drone drone: Record event log for this drone.
        :return: True if operation was success.
        :rtype: bool
        """
        self.custom_logger.info(
            'DroneStatus '
            f'id: {drone.id}, '
            f'serial_number: {drone.serial_number}, '
            f'battery_percent: {drone.battery_percent}, '
            f'status: {drone.status}, '
            f'model: {drone.model}'
        )

        return True
