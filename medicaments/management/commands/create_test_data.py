import inject
from django.conf import settings
from django.core.management.base import BaseCommand

from  medicaments.domain.models import DroneStatusEnum, Load, Drone
from medicaments.domain.factories import LoadFactory, UserFactory, DroneFactory
from medicaments.domain.services import ICreateUserAuthTokens


class Command(BaseCommand):
    """Class command to create a set of data. Be carefully."""

    help = 'Create a set of data for testing.'  # noqa


    def handle(self, *args, **options):  # noqa
        if not settings.DEBUG:
            self.stdout.write(
                self.style.ERROR('DEBUG should be enable for this action.')
            )

        self.create_user()
        self.create_drone_ready_to_load()
        self.create_load()

    def create_load(self) -> Load:
        load = LoadFactory(is_active=True)
        self.print_data(
            label='New load created.',
            value=f'Drone Id: {load.drone.id}'
        )
        return load

    def create_drone_ready_to_load(self) -> Drone:
        limit_weight_grs = 250
        battery_percent = 100
        drone_ready_to_load = DroneFactory(
            status=DroneStatusEnum.IDLE,
            battery_percent=battery_percent,
            limit_weight_grs=limit_weight_grs
        )
        self.print_data(
            label='Drone ready to load',
            value=f'id: {drone_ready_to_load.id} | '
                  f'max weight: {limit_weight_grs}grs | '
                  f'battery: {battery_percent}%) | '
                  f'status: {drone_ready_to_load.status}) | '
                  f'serial_number: {drone_ready_to_load.serial_number})'
        )
        return drone_ready_to_load

    @inject.autoparams()
    def create_user(self, service: ICreateUserAuthTokens):
        user = UserFactory()
        jwt_token = service.execute(user=user)
        self.print_data(label='JWT User', value=jwt_token)
        return user

    def print_data(self, label, value):
        self.stdout.write(self.style.SUCCESS(f'[X] {label}'))
        self.stdout.write(self.style.SUCCESS(value))
        self.stdout.write(self.style.SUCCESS('----'))


