from django.core.management.commands.test import Command as BaseCommand


class Command(BaseCommand):
    """Command instance to update tests execution behaviour."""

    def handle(self, *test_labels, **options):
        """Wrap built-in test command to always delete the database."""
        options['interactive'] = False
        return super().handle(*test_labels, **options)
