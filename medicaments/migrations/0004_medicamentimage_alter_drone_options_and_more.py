# Generated by Django 4.0.3 on 2022-03-17 04:21

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('medicaments', '0003_alter_drone_id_medicament'),
    ]

    operations = [
        migrations.CreateModel(
            name='MedicamentImage',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('image', models.ImageField(upload_to='uploads/medicaments/%Y/%m/%d/')),
            ],
        ),
        migrations.AlterModelOptions(
            name='drone',
            options={'ordering': ['id']},
        ),
        migrations.RemoveField(
            model_name='medicament',
            name='image_id',
        ),
        migrations.AddField(
            model_name='medicament',
            name='image',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='medicaments.medicamentimage'),
        ),
    ]
