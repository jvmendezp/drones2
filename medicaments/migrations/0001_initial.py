# Generated by Django 4.0.3 on 2022-03-12 19:05

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Drone',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('serial_number', models.CharField(max_length=100, unique=True)),
                ('limit_weight_grs', models.PositiveSmallIntegerField()),
                ('battery_percent', models.PositiveSmallIntegerField(default=0)),
                ('status', models.CharField(choices=[('IDLE', 'Idle'), ('LOADING', 'Loading'), ('LOADED', 'Loaded'), ('DELIVERING', 'Delivering'), ('DELIVERED', 'Delivered'), ('RETURNING', 'Returning')], default='IDLE', max_length=20)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
        ),
    ]
