import logging

from django.contrib.auth.models import User
from django.db import models, transaction as db_transaction

logger = logging.getLogger(__name__)


class DroneModelEnum(models.TextChoices):
    """Enum class for available drone models."""

    LIGHTWEIGHT = 'Lightweight'
    MIDDLEWEIGHT = 'Middleweight'
    CRUISERWEIGHT = 'Cruiserweight'
    HEAVYWEIGHT = 'Heavyweight'


class DroneStatusEnum(models.TextChoices):
    """Enum class for available drone statuses."""

    IDLE = 'IDLE'
    LOADING = 'LOADING'
    LOADED = 'LOADED'
    DELIVERING = 'DELIVERING'
    DELIVERED = 'DELIVERED'
    RETURNING = 'RETURNING'

    @classmethod
    def get_loaded_statuses(cls):
        """
        Get drone statuses which indicates that drone is currently loaded.

        :return: Tuple with values which indicates that drone is currently
            loaded.
        :rtype: tuple(DroneStatusEnum)
        """
        return (
            cls.LOADING,
            cls.LOADED,
        )


class Drone(models.Model):
    """Instances of this class represent a drones vehicle."""

    class Meta:
        ordering = ['id']

    MIN_BATTERY_PERCENT = 65
    MAX_WEIGHT = 500

    serial_number = models.CharField(max_length=100, unique=True, null=False)
    limit_weight_grs = models.PositiveSmallIntegerField(null=False)
    battery_percent = models.PositiveSmallIntegerField(default=100)
    status = models.CharField(
        max_length=20,
        choices=DroneStatusEnum.choices,
        default=DroneStatusEnum.IDLE,
    )
    model = models.CharField(
        max_length=20,
        choices=DroneModelEnum.choices,
        null=False,
        default=DroneModelEnum.LIGHTWEIGHT,
    )
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class Load(models.Model):
    """Instances to represent loads."""

    pickup_str = models.CharField(max_length=200, null=False)
    dropoff_str = models.CharField(max_length=200, null=False)
    creator = models.ForeignKey(User, on_delete=models.CASCADE)
    drone = models.ForeignKey(Drone, on_delete=models.SET_NULL, null=True)
    is_active = models.BooleanField(default=False, null=False)

    @db_transaction.atomic()
    def create_with_medicaments(self, medicaments):
        """
        Create current Load instance and its medicaments.

        :param [Medicaments] medicaments: Medicaments for this load.
        :return: Created instance
        :rtype: Load
        """
        self.is_active = True
        self.save()

        self.drone.status = DroneStatusEnum.LOADED
        self.drone.save()

        for medicament in medicaments:
            medicament.load = self
            medicament.save()

        logger.info(f'Crated Load. Id: {self.id}')

        return self


class MedicamentImage(models.Model):
    """This model stores all images."""

    BASE_MEDICAMENT_IMAGES = 'uploads/medicaments'

    image = models.ImageField(
        null=False, upload_to=f'{BASE_MEDICAMENT_IMAGES}/%Y/%m/%d/'
    )


class Medicament(models.Model):
    """Instances to represent medicament."""

    name = models.CharField(max_length=150, null=False)
    weight_gr = models.PositiveSmallIntegerField(null=False)
    code = models.CharField(max_length=50, null=False)
    image = models.ForeignKey(
        MedicamentImage, on_delete=models.SET_NULL, null=True
    )
    load = models.ForeignKey(
        Load, on_delete=models.CASCADE, null=False, related_name='medicaments'
    )

    @property
    def image_path(self):
        """

        :return:
        :rtype: str
        """
        return self.image.image
