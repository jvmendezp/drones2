from abc import ABC, abstractmethod

from django.contrib.auth.models import User
from typing import Generator

from medicaments.domain.models import Drone


class ICreateUserAuthTokens(ABC):
    """Get available drones for loading."""

    @abstractmethod
    def execute(self, user: User) -> str:  # noqa
        """
        Create a new user token for authentication.

        :param User user: Create token for this user.
        :return: JWT string.
        :rtype: str
        """
        raise NotImplementedError()


class IListDronesToCheckBattery(ABC):
    """Return a list of drones to check battery."""

    @abstractmethod
    def execute(self) -> Generator[Drone, None, None]:
        """
        Returns drones pending to check battery.

        :return: Drones to check battery.
        :rtype: Generator[Drone, None, None]
        """
        raise NotImplementedError()


class ICreateBatteryEventLog(ABC):
    """Create a event log for drone battery level."""

    @abstractmethod
    def execute(self, drone: Drone) -> bool:
        """
        Execute logic create event log.

        :param Drone drone: Record event log for this drone.
        :return: True if operation was success.
        :rtype: bool
        """
        raise NotImplementedError()
