import random

import factory
from django.contrib.auth.hashers import make_password
from django.contrib.auth.models import User
from factory import fuzzy  # noqa
from factory.django import DjangoModelFactory

from medicaments.domain.models import (
    Drone,
    DroneStatusEnum,
    Load,
    Medicament,
    MedicamentImage,
)


class UserFactory(DjangoModelFactory):
    """Factory class to build one User instances."""

    class Meta:
        model = User
        django_get_or_create = ('username',)

    first_name = factory.Faker('first_name')
    last_name = factory.Faker('last_name')
    username = 'john'
    email = factory.LazyAttribute(lambda obj: f'{obj.username}@drones.in')
    password = factory.LazyFunction(lambda: make_password('pi3.1415'))
    is_staff = True
    is_superuser = True


class DroneFactory(DjangoModelFactory):
    """Factory class to build random Drone instances."""

    class Meta:
        model = Drone
        django_get_or_create = ('serial_number',)

    serial_number = factory.fuzzy.FuzzyText(prefix='SERIAL_')
    limit_weight_grs = factory.fuzzy.FuzzyInteger(low=1, high=500)
    battery_percent = factory.fuzzy.FuzzyInteger(low=1, high=100)
    status = factory.fuzzy.FuzzyChoice(
        DroneStatusEnum.choices, getter=lambda c: c[0]
    )


class LoadFactory(DjangoModelFactory):
    """Factory class to build random Load instances."""

    class Meta:
        model = Load

    pickup_str = factory.Faker('address')
    dropoff_str = factory.Faker('address')
    drone = DroneFactory(status=DroneStatusEnum.LOADED)
    creator = factory.SubFactory(UserFactory)
    is_active = False

    @factory.post_generation
    def addresses(self, create, extracted, **kwargs):  # noqa
        return MedicamentFactory.create_batch(random.randint(1, 5), load=self)


class MedicamentImageFactory(DjangoModelFactory):
    """Factory class to build random instances MedicamentImage instances."""

    class Meta:
        model = MedicamentImage

    image = f"/fake/path/image_{factory.Faker('first_name')}.jpg"


class MedicamentFactory(DjangoModelFactory):
    """Factory class to build random Medicament instances."""

    class Meta:
        model = Medicament

    name = factory.fuzzy.FuzzyText(prefix='MED_')
    weight_gr = factory.fuzzy.FuzzyInteger(low=1, high=25)
    code = factory.fuzzy.FuzzyText(prefix='COD_')
    load = factory.SubFactory(LoadFactory)
    image = factory.SubFactory(MedicamentImageFactory)
