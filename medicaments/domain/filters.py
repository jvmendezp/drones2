from abc import ABC, abstractmethod

from django.db.models import Q, QuerySet

from medicaments.domain.models import Drone, DroneStatusEnum


class DomainFilter(ABC):
    """Generic class definition for reusable domain filters."""

    @abstractmethod
    def execute(self, qs: QuerySet) -> QuerySet:  # noqa
        """
        Create new Queryset with custom conditions.

        :param QuerySet qs: Base query set to build new one.
        :return: Built queryset.
        :rtype: QuerySet
        """
        raise NotImplementedError()


class DronesForLoadingFilter(DomainFilter):
    """Class with definitions to filter available drones for loading."""

    def execute(self, qs: QuerySet) -> QuerySet:
        """
        Create new Queryset with custom conditions.

        :param QuerySet qs: Base query set to build new one.
        :return: Built queryset.
        :rtype: QuerySet
        """
        return qs.filter(
            Q(
                status=DroneStatusEnum.IDLE,
                battery_percent__gte=Drone.MIN_BATTERY_PERCENT,
            )
        )


class ActiveLoadFilter(DomainFilter):
    """Class with definitions to filter drones with medicaments."""

    def __init__(self, drone: Drone) -> None:
        self.drone = drone

    def execute(self, qs: QuerySet) -> QuerySet:
        """
        Create new Queryset with custom conditions.

        :param QuerySet qs: Base query set to build new one.
        :return: Built queryset.
        :rtype: QuerySet
        """
        return qs.filter(
            Q(
                drone_id=self.drone.id,
                is_active=True,
                drone__status__in=DroneStatusEnum.get_loaded_statuses(),
            )
        )
