import logging

import inject

from medicaments.domain.services import (
    ICreateBatteryEventLog,
    IListDronesToCheckBattery,
)

logger = logging.getLogger(__name__)


@inject.autoparams()
def run(list_drones_srv: IListDronesToCheckBattery,
        create_event_log: ICreateBatteryEventLog) -> None:
    """
    Log information about battery level for drones pending to check.

    :return: None
    """
    for drone in list_drones_srv.execute():
        create_event_log.execute(drone=drone)
