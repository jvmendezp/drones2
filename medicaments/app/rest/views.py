import inject
from django.http import Http404
from django.shortcuts import get_object_or_404 as _get_object_or_404
from rest_framework.generics import CreateAPIView, ListAPIView, RetrieveAPIView
from rest_framework.permissions import IsAuthenticated

from medicaments.app.rest.serializers import (
    DroneSerializer,
    LoadSerializer,
    MedicamentImageSerializer,
)
from medicaments.domain.filters import ActiveLoadFilter, DronesForLoadingFilter
from medicaments.domain.models import Drone, Load, MedicamentImage


class AuthSettings:
    """Base class to configure views authentication."""

    permission_classes = [IsAuthenticated]


class ListAvailableForLoading(AuthSettings, ListAPIView):
    """List available droned for loading."""

    queryset = Drone.objects.filter()
    serializer_class = DroneSerializer

    @inject.autoparams()
    def get_queryset(self, drones_for_loading_filter: DronesForLoadingFilter):
        """
        Built new queryset with custom option.

        :param DronesForLoadingFilter drones_for_loading_filter: Filter to
            crete new queryset with custom filter options.
        :return: QuerySet with options to list available drones for loading.
        :rtype: QuerySet
        """
        return drones_for_loading_filter.execute(
            qs=super(ListAvailableForLoading, self).get_queryset()
        )


class CreateDrone(AuthSettings, CreateAPIView):
    """Create a drone."""

    serializer_class = DroneSerializer


class RetrieveDrone(AuthSettings, RetrieveAPIView):
    """Retrieve a drone with all its information(include battery level)."""

    queryset = Drone.objects.all()
    serializer_class = DroneSerializer


class CreateMedicamentImageView(AuthSettings, CreateAPIView):
    """Create a new medicament image."""

    queryset = MedicamentImage.objects.all()
    serializer_class = MedicamentImageSerializer


class CreateLoad(AuthSettings, CreateAPIView):
    """Loading a drone with medication items."""

    serializer_class = LoadSerializer
    queryset = Load.objects.prefetch_related('medicaments')

    def get_serializer_context(self):
        """
        Extra context provided to the serializer class.

        :return: Context data for serializer.
        :rtype: dict
        """
        context = super(CreateLoad, self).get_serializer_context()
        context.update(
            {'drone': self.get_drone(), 'creator': self.request.user}
        )
        return context

    @inject.autoparams()
    def get_drone(self, drones_for_loading_filter: DronesForLoadingFilter):
        """
        Get drone from url parameter.

        :param DronesForLoadingFilter drones_for_loading_filter:
        :return:
        :rtype: Drone
        """
        drone_id = self.kwargs.get(self.lookup_field)
        results = drones_for_loading_filter.execute(
            qs=Drone.objects.filter(pk=drone_id)
        )

        if not results:
            raise Http404('Drone not available.')

        return results.first()


class RetrieveLoadedDrone(AuthSettings, RetrieveAPIView):
    """Retrieve current drone load."""

    queryset = Load.objects.prefetch_related('medicaments')
    serializer_class = LoadSerializer

    def get_object(self):
        """
        Return the object the view is displaying.

        :return: Active load instance for specified drone.
        :rtype: Load
        """
        drone_id = self.kwargs.get(self.lookup_field)
        drone = Drone.objects.get(pk=drone_id)
        loaded_drones_filter = ActiveLoadFilter(drone=drone)
        queryset = loaded_drones_filter.execute(
            qs=self.filter_queryset(self.get_queryset())
        )
        obj = _get_object_or_404(queryset)

        return obj
