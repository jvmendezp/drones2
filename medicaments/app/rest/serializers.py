from django.contrib.auth.models import User
from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from rest_framework.validators import UniqueValidator

from medicaments.domain.models import Drone, Load, Medicament, MedicamentImage


class UserSerializer(serializers.ModelSerializer):
    """Basic serializer for Drone instance."""

    class Meta:
        model = User
        fields = ['id']


class DroneSerializer(serializers.ModelSerializer):
    """Basic serializer for Drone instance."""

    class Meta:
        model = Drone
        fields = [
            'id',
            'serial_number',
            'limit_weight_grs',
            'battery_percent',
            'status',
            'model',
        ]
        read_only_fields = (
            'id',
            'battery_percent',
            'status',
        )

    serial_number = serializers.CharField(
        allow_blank=False,
        max_length=100,
        validators=[
            UniqueValidator(queryset=Drone.objects.all(), lookup='iexact')
        ],
    )
    limit_weight_grs = serializers.IntegerField(max_value=500, min_value=1)


class MedicamentImageSerializer(serializers.ModelSerializer):
    """Class to serialize medicament image domain model."""

    class Meta:
        model = MedicamentImage
        fields = (
            'id',
            'image',
        )

    image = serializers.ImageField()


class MedicamentSerializer(serializers.ModelSerializer):
    """Class to serialize Medicament instance."""

    class Meta:
        model = Medicament
        fields = [
            'name',
            'weight_gr',
            'code',
            'image',
            'image_path',
        ]

    name = serializers.RegexField(
        regex=r'^[a-z-0-9A-Z_\-]+$', required=True, max_length=100
    )
    weight_gr = serializers.IntegerField(max_value=500, min_value=1)
    code = serializers.RegexField(
        regex=r'^[A-Z0-9_]+$', required=True, max_length=50
    )
    image = serializers.PrimaryKeyRelatedField(
        queryset=MedicamentImage.objects.all(), write_only=True
    )
    image_path = serializers.ImageField(read_only=True)


class LoadSerializer(serializers.ModelSerializer):
    """Class to serialize Load instance."""

    class Meta:
        model = Load
        fields = [
            'id',
            'pickup_str',
            'dropoff_str',
            'medicaments',
        ]

    pickup_str = serializers.CharField(required=True, max_length=200)
    dropoff_str = serializers.CharField(required=True, max_length=200)
    medicaments = MedicamentSerializer(many=True)

    def validate_medicaments(self, data):
        """
        Check if medication weight does not exceed drone limit.

        Business requeriment:
            "checking loaded medication items for a given drone;"

        :param dict data: medicament field value.
        :return: Field value if all is ok.
        :rtype: dict
        :raise: serializers.ValidationError
        """
        drone_limits = self.context['drone'].limit_weight_grs
        total_weight = 0
        for medication_data in data:
            total_weight += int(medication_data['weight_gr'])
            if total_weight > drone_limits:
                raise serializers.ValidationError(
                    f'Medication weights exceeds drone '
                    f'limits: {drone_limits}grs.'
                )
        return data

    def create(self, validated_data):
        """
        Redefine logic for create due to existing model relations.

        :param dict validated_data: Entry data.
        :return: Created instance of Load.
        :rtype: Load
        """
        validated_data['drone'] = self.context['drone']
        validated_data['creator'] = self.context['creator']

        medicaments = [
            Medicament(**medicament_data,)
            for medicament_data in validated_data.pop('medicaments')
        ]

        load = Load(**validated_data)

        try:
            load.create_with_medicaments(medicaments=medicaments)
        except:  # noqa
            raise ValidationError()
        return load
