"""Configuration urls path for v1 version of medicaments module."""

from django.urls import include, path

from medicaments.app.rest import views

urlpatterns = [
    path('', views.CreateDrone.as_view()),
    path('medicament_images', views.CreateMedicamentImageView.as_view()),
    path('availables', views.ListAvailableForLoading.as_view()),
    path(
        '<int:pk>/',
        include(
            [
                path('', views.RetrieveDrone.as_view()),
                path(
                    'loads/',
                    include(
                        [
                            path(
                                'current', views.RetrieveLoadedDrone.as_view()
                            ),
                            path('', views.CreateLoad.as_view()),
                        ]
                    ),
                ),
            ]
        ),
    ),
]
