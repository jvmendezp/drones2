import importlib

import inject
from django.apps import AppConfig


class MedicamentsConfig(AppConfig):

    default_auto_field = 'django.db.models.BigAutoField'
    name = 'medicaments'

    PROVIDERS = [
        # SERVICES.
        (
            'medicaments.domain.services.ICreateUserAuthTokens',
            'medicaments.infraestructure.services.CreateUserAuthTokensWithSimpleJWT',  # noqa
        ),
        (
            'medicaments.domain.services.IListDronesToCheckBattery',
            'medicaments.infraestructure.services.ListAllDronesToCheckBattery',
        ),
        (
            'medicaments.domain.services.ICreateBatteryEventLog',
            'medicaments.infraestructure.services.CreateBatteryEventLogging',
        ),
    ]

    def ready(self):
        """
        Execute logic when application starts.

        :return: None
        """
        self.setup_injection()

    @staticmethod
    def setup_injection():
        inject.configure_once(MedicamentsConfig.config_inject)

    def config_inject(binder):
        """
        Configure dependency injection

        See: https://pypi.org/project/Inject/
        :param inject.Binder binder: Inject binder instance.
        :return: None
        """

        def get_class_from_path(full_path):
            module_path, class_name = full_path.rsplit('.', 1)
            return getattr(importlib.import_module(module_path), class_name)

        for interface, implementation in MedicamentsConfig.PROVIDERS:
            binder.bind_to_provider(
                get_class_from_path(interface),
                get_class_from_path(implementation),
            )
