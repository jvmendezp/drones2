from django.test import TestCase

from medicaments.domain import factories
from medicaments.domain.filters import ActiveLoadFilter, DronesForLoadingFilter
from medicaments.domain.models import Drone, DroneStatusEnum, Load


class TestDronesForLoadingFilter(TestCase):
    """Tests for logic into DronesForLoadingFilter."""

    def test_check_return_correct_data(self):
        """Check if filter created correct queryset."""
        factories.DroneFactory(status=DroneStatusEnum.IDLE, battery_percent=1)
        drone_idle_high_battery = factories.DroneFactory(
            status=DroneStatusEnum.IDLE, battery_percent=100
        )
        factories.DroneFactory(
            status=DroneStatusEnum.LOADING, battery_percent=100
        )
        factories.DroneFactory(
            status=DroneStatusEnum.LOADING, battery_percent=1
        )

        drones_filter = DronesForLoadingFilter()

        results = list(drones_filter.execute(qs=Drone.objects.all()))

        self.assertEqual(1, len(results))
        self.assertIn(drone_idle_high_battery, results)


class TestActiveLoadFilterFilter(TestCase):
    """Tests for logic into CurrentlyLoadedDronesFilter."""

    def test_check_return_correct_data(self):
        """Check if filter created correct queryset."""
        # Create a drone with an old loads.
        idle_drone = factories.DroneFactory(status=DroneStatusEnum.IDLE)
        factories.LoadFactory(drone=idle_drone)
        factories.LoadFactory(drone=idle_drone)

        # Create a drone with active load and another old load.
        loaded_drone = factories.DroneFactory(status=DroneStatusEnum.LOADING)
        factories.LoadFactory(drone=loaded_drone)
        current_load = factories.LoadFactory(
            drone=loaded_drone, is_active=True
        )

        drones_filter = ActiveLoadFilter(drone=loaded_drone)
        results = list(drones_filter.execute(qs=Load.objects.all()))

        self.assertEqual(results, [current_load])
