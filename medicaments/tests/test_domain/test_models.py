from django.test import TestCase

from medicaments.domain.models import Drone, DroneStatusEnum, Load, Medicament


class TestDroneStatusEnum(TestCase):
    """Tests for logic into DroneStatusEnum class."""

    def test_idle(self):
        """Check if enum value(IDLE) definition is correct."""
        self.assertEqual(DroneStatusEnum.IDLE, 'IDLE')

    def test_loading(self):
        """Check if enum value(LOADING) definition is correct."""
        self.assertEqual(DroneStatusEnum.LOADING, 'LOADING')

    def test_loaded(self):
        """Check if enum value(LOADED) definition is correct."""
        self.assertEqual(DroneStatusEnum.LOADED, 'LOADED')

    def test_delivering(self):
        """Check if enum value(DELIVERING) definition is correct."""
        self.assertEqual(DroneStatusEnum.DELIVERING, 'DELIVERING')

    def test_delivered(self):
        """Check if enum value(DELIVERED) definition is correct."""
        self.assertEqual(DroneStatusEnum.DELIVERED, 'DELIVERED')

    def test_returning(self):
        """Check if enum value(RETURNING) definition is correct."""
        self.assertEqual(DroneStatusEnum.RETURNING, 'RETURNING')


class TestDrone(TestCase):
    """Tests for Drone class."""

    def test_check_min_battery_percent(self):
        """Check min battery percent value."""
        self.assertEqual(Drone.MIN_BATTERY_PERCENT, 65)

    def test_basic_init(self):
        """Basic tests for instance initialization."""
        mock_id = 1
        mock_serial_number = 'serialMockValue'
        mock_limit_weight_grs = 100
        mock_battery_percent = 68
        mock_status = DroneStatusEnum.LOADING

        drone = Drone(
            id=mock_id,
            serial_number=mock_serial_number,
            limit_weight_grs=mock_limit_weight_grs,
            battery_percent=mock_battery_percent,
            status=mock_status,
        )

        self.assertEqual(drone.id, mock_id)
        self.assertEqual(drone.serial_number, mock_serial_number)
        self.assertEqual(drone.limit_weight_grs, mock_limit_weight_grs)
        self.assertEqual(drone.battery_percent, mock_battery_percent)
        self.assertEqual(drone.status, mock_status)


class TestLoad(TestCase):
    """Tests for logic in Load model."""

    def test_basic_init(self):
        """Basic tests for instance initialization."""
        mock_id = 1
        mock_pickup_str = 'mock_pickup_str'
        mock_dropoff_str = 'mock_dropoff_str'

        load = Load(
            id=mock_id,
            pickup_str=mock_pickup_str,
            dropoff_str=mock_dropoff_str,
        )

        self.assertEqual(load.id, mock_id)
        self.assertEqual(load.pickup_str, mock_pickup_str)
        self.assertEqual(load.dropoff_str, mock_dropoff_str)


class TestMedicament(TestCase):
    """Tests for logic in Medicament model."""

    def test_basic_init(self):
        """Basic tests for instance initialization."""
        mock_id = 1
        mock_name = 'mock_name'
        mock_weight_gr = 150
        mock_code = 'mock_code'

        load = Medicament(
            id=mock_id,
            name=mock_name,
            weight_gr=mock_weight_gr,
            code=mock_code,
        )

        self.assertEqual(load.id, mock_id)
        self.assertEqual(load.name, mock_name)
        self.assertEqual(load.weight_gr, mock_weight_gr)
        self.assertEqual(load.code, mock_code)
