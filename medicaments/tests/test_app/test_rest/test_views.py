import json
import tempfile

import inject
from django.test import TransactionTestCase
from PIL import Image
from rest_framework import status
from rest_framework.test import APIClient

from medicaments.domain import factories
from medicaments.domain.models import (
    DroneModelEnum,
    DroneStatusEnum,
    Load,
    MedicamentImage,
)
from medicaments.domain.services import ICreateUserAuthTokens


class TestListAvailableForLoading(TransactionTestCase):
    """Tests to check uses cases for ListAvailableForLoading view."""

    API_URL_PATH = '/v1/drones/availables'

    @inject.autoparams()
    def setUp(self, create_auth_token: ICreateUserAuthTokens) -> None:
        """Prepare data for tests."""
        user = factories.UserFactory()
        self.client = APIClient()
        self.client.credentials(
            HTTP_AUTHORIZATION=f'Bearer {create_auth_token.execute(user=user)}'
        )

    def test_auth(self):
        """Check if endpoint is not available for anonymous user."""
        client = APIClient()
        response = client.get(self.API_URL_PATH, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_return_only_valid_drone(self):
        """Check if view filter applies correct conditions."""
        factories.DroneFactory(status=DroneStatusEnum.IDLE, battery_percent=1)
        drone_idle_high_battery = factories.DroneFactory(
            status=DroneStatusEnum.IDLE, battery_percent=100
        )
        factories.DroneFactory(
            status=DroneStatusEnum.LOADING, battery_percent=100
        )
        factories.DroneFactory(
            status=DroneStatusEnum.LOADING, battery_percent=1
        )

        response = self.client.get(self.API_URL_PATH, format='json')
        response_dict = json.loads(response.content)
        result_dict = response_dict['results'][0]

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(1, response_dict['count'])
        self.assertDictEqual(
            {
                'id': drone_idle_high_battery.id,
                'serial_number': drone_idle_high_battery.serial_number,
                'limit_weight_grs': drone_idle_high_battery.limit_weight_grs,
                'model': drone_idle_high_battery.model,
                'battery_percent': drone_idle_high_battery.battery_percent,
                'status': DroneStatusEnum.IDLE,
            },
            result_dict,
        )


class TestCreateDrone(TransactionTestCase):
    """Tests to check uses cases for CreateDrone view."""

    API_URL_PATH = '/v1/drones/'

    @inject.autoparams()
    def setUp(self, create_auth_token: ICreateUserAuthTokens) -> None:
        """Prepare data for tests."""
        user = factories.UserFactory()
        self.client = APIClient()
        self.client.credentials(
            HTTP_AUTHORIZATION=f'Bearer {create_auth_token.execute(user=user)}'
        )

    def test_auth(self):
        """Check if endpoint is not available for anonymous user."""
        client = APIClient()
        response = client.post(self.API_URL_PATH, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_create_drone_success(self):
        """Check drone is correctly created."""
        mock_serial_number = 'MOCK_SERIAL_NUMBER'
        mock_limit_weight_grs = 450
        mock_model = DroneModelEnum.MIDDLEWEIGHT

        data = {
            'serial_number': mock_serial_number,
            'limit_weight_grs': mock_limit_weight_grs,
            'model': mock_model,
        }

        response = self.client.post(self.API_URL_PATH, data, format='json')

        response_dict = json.loads(response.content)

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response_dict['serial_number'], mock_serial_number)
        self.assertEqual(
            response_dict['limit_weight_grs'], mock_limit_weight_grs
        )
        self.assertEqual(response_dict['battery_percent'], 100)
        self.assertEqual(response_dict['status'], DroneStatusEnum.IDLE)
        self.assertEqual(response_dict['model'], mock_model)

    def test_create_drone_fails_long_serial_number(self):
        """Check response when serial number is long."""
        mock_serial_number = 'A' * 101
        mock_limit_weight_grs = 450

        data = {
            'serial_number': mock_serial_number,
            'limit_weight_grs': mock_limit_weight_grs,
        }

        response = self.client.post(self.API_URL_PATH, data, format='json')
        response_dict = json.loads(response.content)

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertIn('serial_number', response_dict)

    def test_create_drone_fails_empty_serial_number(self):
        """Check response when serial number is empty."""
        mock_limit_weight_grs = 450

        data = {'limit_weight_grs': mock_limit_weight_grs}

        response = self.client.post(self.API_URL_PATH, data, format='json')
        response_dict = json.loads(response.content)

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertIn('serial_number', response_dict)

    def test_create_drone_fails_due_invalid_model(self):
        """Check response when model is invalid."""
        data = {
            'serial_number': 'serial',
            'limit_weight_grs': 20,
            'model': 'other_model',
        }

        response = self.client.post(self.API_URL_PATH, data, format='json')
        response_dict = json.loads(response.content)

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertIn('model', response_dict)

    def test_create_drone_fails_exists_serial_number(self):
        """Check response when serial number exists."""
        drone = factories.DroneFactory()
        data = {
            'limit_weight_grs': 450,
            'serial_number': drone.serial_number.upper(),
        }

        response = self.client.post(self.API_URL_PATH, data, format='json')
        response_dict = json.loads(response.content)

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertIn('serial_number', response_dict)

    def test_create_drone_fails_high_limit_weight_grs(self):
        """Check response when limit_weight_grs is higher than limit."""
        mock_serial_number = 'MOCK_SERIAL_NUMBER'
        mock_limit_weight_grs = 900

        data = {
            'serial_number': mock_serial_number,
            'limit_weight_grs': mock_limit_weight_grs,
        }

        response = self.client.post(self.API_URL_PATH, data, format='json')
        response_dict = json.loads(response.content)

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertIn('limit_weight_grs', response_dict)

    def test_create_drone_fails_empty_limit_weight_grs(self):
        """Check response when limit_weight_grs is empty."""
        mock_serial_number = 'MOCK_SERIAL_NUMBER'

        data = {'serial_number': mock_serial_number}

        response = self.client.post(self.API_URL_PATH, data, format='json')
        response_dict = json.loads(response.content)

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertIn('limit_weight_grs', response_dict)


class TestRetrieveDrone(TransactionTestCase):
    """Tests to check uses cases for RetrieveDrone view."""

    API_URL_PATH = '/v1/drones/{id}/'

    @inject.autoparams()
    def setUp(self, create_auth_token: ICreateUserAuthTokens) -> None:
        """Prepare data for tests."""
        user = factories.UserFactory()
        self.client = APIClient()
        self.client.credentials(
            HTTP_AUTHORIZATION=f'Bearer {create_auth_token.execute(user=user)}'
        )

    def test_auth(self):
        """Check if endpoint is not available for anonymous user."""
        client = APIClient()
        response = client.get(self.API_URL_PATH.format(id=1), format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_return_valid_drone(self):
        """Check if endpoint returns correct Drone instance."""
        drone = factories.DroneFactory()
        factories.DroneFactory()
        factories.DroneFactory()

        response = self.client.get(
            self.API_URL_PATH.format(id=drone.id), format='json'
        )
        response_dict = json.loads(response.content)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertDictEqual(
            {
                'id': drone.id,
                'serial_number': drone.serial_number,
                'limit_weight_grs': drone.limit_weight_grs,
                'model': drone.model,
                'battery_percent': drone.battery_percent,
                'status': drone.status,
            },
            response_dict,
        )

    def test_response_when_drone_does_not_exists(self):
        """Check response when service receive an invalid id."""
        response = self.client.get(
            self.API_URL_PATH.format(id=100), format='json'
        )

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


class TestCreateMedicamentImageView(TransactionTestCase):
    """Tests to check uses cases for CreateMedicamentImageView view."""

    API_URL_PATH = '/v1/drones/medicament_images'

    @inject.autoparams()
    def setUp(self, create_auth_token: ICreateUserAuthTokens) -> None:
        """Prepare data for tests."""
        user = factories.UserFactory()
        self.client = APIClient()
        self.client.credentials(
            HTTP_AUTHORIZATION=f'Bearer {create_auth_token.execute(user=user)}'
        )

    @staticmethod
    def get_temporary_image(temp_file):
        """Create a image at specified path."""
        size = (200, 200)
        color = (255, 0, 0, 0)
        image = Image.new('RGB', size, color)
        image.save(temp_file, 'jpeg')
        return temp_file

    def test_auth(self):
        """Check if endpoint is not available for anonymous user."""
        client = APIClient()
        response = client.post(self.API_URL_PATH)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_create_image_success(self):
        """Check medicament image is correctly created."""
        with tempfile.NamedTemporaryFile(suffix='.jpg') as temp_image:
            self.get_temporary_image(temp_file=temp_image)
            response = self.client.post(
                self.API_URL_PATH,
                data={
                    'image': open(temp_image.name, 'rb'),
                },
                format='multipart',
            )
        response_dict = json.loads(response.content)
        instance_db = MedicamentImage.objects.get(pk=response_dict['id'])

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertTrue(
            response_dict['image'].endswith(instance_db.image.name)
        )

    def test_create_medicament_images_fails_empty_image(self):
        """Check response when image is empty."""
        with tempfile.NamedTemporaryFile(suffix='.jpg') as temp_image:
            self.get_temporary_image(temp_file=temp_image)
            response = self.client.post(
                self.API_URL_PATH, data={'image': ''}, format='multipart'
            )
        response_dict = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertTrue('image' in response_dict)


class TestCreateLoad(TransactionTestCase):
    """Tests to uses cases for CreateLoad view."""

    API_URL_PATH = '/v1/drones/{id}/loads/'
    MAX_DRONE_WEIGHT = 400

    @inject.autoparams()
    def setUp(self, create_auth_token: ICreateUserAuthTokens) -> None:
        """Prepare test environment."""
        self.user = factories.UserFactory()
        self.client = APIClient()
        self.client.credentials(
            HTTP_AUTHORIZATION=f'Bearer '
            f'{create_auth_token.execute(user=self.user)}'
        )
        self.drone = factories.DroneFactory(
            limit_weight_grs=self.MAX_DRONE_WEIGHT,
            battery_percent=98,
            status=DroneStatusEnum.IDLE,
        )

    def test_create_load_success(self):
        """Check a los is correctly created."""
        mock_pickup = 'pickup_address'
        mock_dropoff_str = 'mock_dropoff'

        response = self.client.post(
            self.API_URL_PATH.format(id=self.drone.id),
            data={
                'pickup_str': mock_pickup,
                'dropoff_str': mock_dropoff_str,
                'medicaments': [
                    {
                        'name': 'Mockmed_name1Z-',
                        'weight_gr': 50,
                        'code': 'CODE_MED_1',
                        'image': factories.MedicamentImageFactory().id,
                    },
                    {
                        'name': 'Mock_med-namer2',
                        'weight_gr': 60,
                        'code': 'CODE_MED_2',
                        'image': factories.MedicamentImageFactory().id,
                    },
                ],
            },
            format='json',
        )
        response_dict = json.loads(response.content)
        new_load_id = response_dict['id']

        load_instance = Load.objects.get(pk=new_load_id)

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response_dict['pickup_str'], mock_pickup)
        self.assertEqual(response_dict['dropoff_str'], mock_dropoff_str)
        self.assertEqual(load_instance.creator_id, self.user.id)
        self.assertEqual(load_instance.drone_id, self.drone.id)
        self.assertEqual(len(response_dict['medicaments']), 2)
        self.assertEqual(load_instance.drone.status, DroneStatusEnum.LOADED)
        self.assertTrue(load_instance.is_active)

    def test_create_fails_due_medication_name(self):
        """Check response when name has invalid characters."""
        invalid_medication_name = 'Med1cati0n %'
        response = self.client.post(
            self.API_URL_PATH.format(id=self.drone.id),
            data={
                'pickup_str': 'pickup_address',
                'dropoff_str': 'mock_dropoff',
                'medicaments': [
                    {
                        'name': invalid_medication_name,
                        'weight_gr': 50,
                        'code': 'CODEMEDONE',
                        'image': factories.MedicamentImageFactory().id,
                    },
                ],
            },
            format='json',
        )
        response_dict = json.loads(response.content)

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertIn('name', response_dict['medicaments'][0])

    def test_create_fails_due_medication_weight(self):
        """Check response when weight exceeds 500gr."""
        response = self.client.post(
            self.API_URL_PATH.format(id=self.drone.id),
            data={
                'pickup_str': 'pickup_address',
                'dropoff_str': 'mock_dropoff',
                'medicaments': [
                    {
                        'name': 'medication_name',
                        'weight_gr': 501,
                        'code': 'CODEMEDONE',
                        'image': factories.MedicamentImageFactory().id,
                    },
                ],
            },
            format='json',
        )
        response_dict = json.loads(response.content)

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertIn('weight_gr', response_dict['medicaments'][0])

    def test_create_fails_due_medication_code(self):
        """Check response when code has invalid characters."""
        response = self.client.post(
            self.API_URL_PATH.format(id=self.drone.id),
            data={
                'pickup_str': 'pickup_address',
                'dropoff_str': 'mock_dropoff',
                'medicaments': [
                    {
                        'name': 'medication_name',
                        'weight_gr': 501,
                        'code': 'code1',
                        'image': factories.MedicamentImageFactory().id,
                    },
                ],
            },
            format='json',
        )
        response_dict = json.loads(response.content)

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertIn('code', response_dict['medicaments'][0])

    def test_create_fails_due_medication_image_id(self):
        """Check response when image_id does not exist."""
        response = self.client.post(
            self.API_URL_PATH.format(id=self.drone.id),
            data={
                'pickup_str': 'pickup_address',
                'dropoff_str': 'mock_dropoff',
                'medicaments': [
                    {
                        'name': 'medication_name',
                        'weight_gr': 200,
                        'code': 'CODE',
                        'image': 200,
                    },
                ],
            },
            format='json',
        )
        response_dict = json.loads(response.content)

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertIn('image', response_dict['medicaments'][0])

    def test_create_fails_due_all_medication_weight(self):
        """Check response when medications weights exceeds drone limit."""
        response = self.client.post(
            self.API_URL_PATH.format(id=self.drone.id),
            data={
                'pickup_str': 'pickup_address',
                'dropoff_str': 'mock_dropoff',
                'medicaments': [
                    {
                        'name': 'medication_name',
                        'weight_gr': self.MAX_DRONE_WEIGHT,
                        'code': 'CODE',
                        'image': factories.MedicamentImageFactory().id,
                    },
                    {
                        'name': 'medication_name',
                        'weight_gr': 1,
                        'code': 'CODE',
                        'image': factories.MedicamentImageFactory().id,
                    },
                ],
            },
            format='json',
        )
        response_dict = json.loads(response.content)

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertIn('medicaments', response_dict)

    def test_create_fails_due_drone_not_available(self):
        """
        Check response when specified drone is not available.

        Requirement:
            "Prevent the drone from being in LOADING state if the battery 
            level is **below 25**"
        """
        drone_not_available = factories.DroneFactory(
            limit_weight_grs=self.MAX_DRONE_WEIGHT,
            battery_percent=24,
            status=DroneStatusEnum.IDLE,
        )
        response = self.client.post(
            self.API_URL_PATH.format(id=drone_not_available.id),
            data={
                'pickup_str': 'pickup_address',
                'dropoff_str': 'mock_dropoff',
                'medicaments': [
                    {
                        'name': 'medication_name',
                        'weight_gr': 10,
                        'code': 'CODE',
                        'image': factories.MedicamentImageFactory().id,
                    },
                ],
            },
            format='json',
        )
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


class TestRetrieveLoadedDrone(TransactionTestCase):
    """Tests to uses cases for RetrieveLoadedDrone view."""

    API_URL_PATH = '/v1/drones/{id}/loads/current'
    MAX_DRONE_WEIGHT = 400

    @inject.autoparams()
    def setUp(self, create_auth_token: ICreateUserAuthTokens) -> None:
        """Prepare test environment."""
        self.user = factories.UserFactory()
        self.client = APIClient()
        self.client.credentials(
            HTTP_AUTHORIZATION=f'Bearer '
            f'{create_auth_token.execute(user=self.user)}'
        )
        self.drone = factories.DroneFactory(
            limit_weight_grs=self.MAX_DRONE_WEIGHT,
            battery_percent=98,
            status=DroneStatusEnum.IDLE,
        )

    def test_return_valid_load(self):
        """Check if endpoint returns correct Load instance."""
        # Create a drone with an old loads.
        idle_drone = factories.DroneFactory(status=DroneStatusEnum.IDLE)
        factories.LoadFactory(drone=idle_drone)
        factories.LoadFactory(drone=idle_drone)

        # Create a drone with active load and another old load.
        loaded_drone = factories.DroneFactory(status=DroneStatusEnum.LOADING)
        factories.LoadFactory(drone=loaded_drone)
        current_load = factories.LoadFactory(
            drone=loaded_drone, is_active=True
        )

        response = self.client.get(
            self.API_URL_PATH.format(id=loaded_drone.id), format='json'
        )
        response_dict = json.loads(response.content)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response_dict['id'], current_load.id)
        self.assertIsNotNone(response_dict['medicaments'])
