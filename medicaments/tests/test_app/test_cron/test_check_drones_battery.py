import inject
from django.test import TestCase
from mock import call, MagicMock

from medicaments.app.cron import check_drones_battery
from medicaments.apps import MedicamentsConfig
from medicaments.domain.services import (
    ICreateBatteryEventLog,
    IListDronesToCheckBattery,
)


class TestRun(TestCase):
    """Tests for logic into run() function."""

    def setUp(self):
        """Prepate tests."""
        self.mock_list_drones_srv = MagicMock()
        self.mock_create_event_srv = MagicMock()
        inject.clear_and_configure(
            lambda binder: binder.bind(
                IListDronesToCheckBattery, self.mock_list_drones_srv
            ).bind(ICreateBatteryEventLog, self.mock_create_event_srv)
        )

    def tearDown(self):
        """Deconstructing tests preparation."""
        inject.clear()
        MedicamentsConfig.setup_injection()

    def test_check_if_all_method_are_called(self):
        """Check if logic works as expected."""
        drone_1, drone_2, drone_3 = MagicMock(), MagicMock(), MagicMock()
        self.mock_list_drones_srv.execute.return_value = (
            drone_1,
            drone_2,
            drone_3,
        )

        check_drones_battery.run()

        self.mock_create_event_srv.execute.assert_has_calls(
            [call(drone=drone_1), call(drone=drone_2), call(drone=drone_3)]
        )
