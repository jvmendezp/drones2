from django.test import TestCase
from mock import MagicMock, patch

from medicaments.domain.factories import DroneFactory
from medicaments.domain.models import DroneStatusEnum
from medicaments.infraestructure.services import (
    CreateBatteryEventLogging,
    CreateUserAuthTokensWithSimpleJWT,
    ListAllDronesToCheckBattery,
)


class TestCreateUserAuthTokensWithSimpleJWT(TestCase):
    """Tests for logic in CreateUserAuthTokensWithSimpleJWT class."""

    def test_returns_correct_data_from_simplejwt(self):
        """Check if method returns correct data from third library."""
        service = CreateUserAuthTokensWithSimpleJWT()
        mock_user = MagicMock()

        mock_expected_token = MagicMock()
        mock_expected_refresh = MagicMock()
        mock_expected_refresh.access_token = mock_expected_token

        with patch(
                'rest_framework_simplejwt.tokens.RefreshToken.for_user'
        ) as mock_for_user:
            mock_for_user.return_value = mock_expected_refresh
            response = service.execute(user=mock_user)
            mock_for_user.assert_called_with(user=mock_user)
            self.assertEqual(mock_expected_token, response)


class TestListAllDronesToCheckBattery(TestCase):
    """Tests for logic into ListAllDronesToCheckBattery class."""

    def test_return_all_items(self):
        """Check is all items are returned."""
        drone_1, drone_2 = DroneFactory(), DroneFactory()

        service = ListAllDronesToCheckBattery()

        results = list(service.execute())

        self.assertEqual(2, len(results))
        self.assertIn(drone_1, results)
        self.assertIn(drone_2, results)


class TestCreateBatteryEventLogging(TestCase):
    """Tests for logic into CreateBatteryEventLogging class."""

    def test_check_log_message(self):
        mock_id = 2
        mock_serial_number = 'SERIAL'
        mock_battery_percent = 35
        mock_status = DroneStatusEnum.IDLE
        mock_model = DroneStatusEnum.IDLE

        mock_drone = MagicMock(
            id=mock_id, serial_number=mock_serial_number,
            battery_percent=mock_battery_percent, status=mock_status,
            model=mock_model
        )

        mock_logger = MagicMock()

        with patch('medicaments.infraestructure.services.CreateBatteryEventLogging.get_logger') as mock_get_logger:
            mock_get_logger.return_value = mock_logger
            service = CreateBatteryEventLogging()
            service.execute(drone=mock_drone)
            mock_logger.info.assert_called_with(
                f'DroneStatus id: {mock_id}, '
                f'serial_number: {mock_serial_number}, '
                f'battery_percent: {mock_battery_percent}, '
                f'status: {mock_status}, '
                f'model: {mock_model}'
            )
